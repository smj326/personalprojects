/*********************************************
Sean Jamison
sean.jamison@rocketmail.com

Ascii Based Chess Client
Works based on FEN and input is in algebraic notation

more information within the README
**********************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "squareNames.h"

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"


//This chess implementation will be using Forsyth–Edwards Notation
//more information on how it is constructed can be found here
//https://en.wikipedia.org/wiki/Forsyth%E2%80%93Edwards_Notation
//
//uppercase is white, lower is black
//white is cyan, black is red

struct FEN
	{
	char piecePlacement[100];
	char activeColor;
	char castling[4];
	char enPassant[3];
	int halfMoveClock;
	int fullMoveNumber;
	};

struct boardSquare
	{
	struct boardSquare* up;
	struct boardSquare* down;
	struct boardSquare* left;
	struct boardSquare* right;
	char piece;
	};

void printFEN(struct FEN* boardFEN)
{
	printf("%s %c %s %s %d %d\n\n",boardFEN->piecePlacement,boardFEN->activeColor,boardFEN->castling,boardFEN->enPassant,boardFEN->halfMoveClock,boardFEN->fullMoveNumber);
}

void FENtoBoard(struct FEN* boardFEN, struct boardSquare* board)
{
	int position = 0;
	for(int i = 0; i < strlen(boardFEN->piecePlacement); i++)
	{
		if(isalpha(boardFEN->piecePlacement[i]))
		{
			board[position].piece = boardFEN->piecePlacement[i];
			position++;
		}
		else if(isdigit(boardFEN->piecePlacement[i]))
			position = position + (int) boardFEN->piecePlacement[i] - '0';
	}
}

void printBoardFEN(struct FEN* boardFEN)
{
	printf("\n    a   b   c   d   e   f   g   h\n  |-------------------------------|\n8 ");
	int row = 7;
	for(int i = 0; i<strlen(boardFEN->piecePlacement); i++)
	{
		printf("|");
		if(boardFEN->piecePlacement[i]=='/')
		{
			printf("\n  |-------------------------------|\n%d ",row);
			row--;
		}
		else if(isdigit(boardFEN->piecePlacement[i]))
		{
			for(int j = 0; j < (int) boardFEN->piecePlacement[i]-'0'; j++)
					printf("   |");
			printf("\b");
		}
		else if(isalpha(boardFEN->piecePlacement[i]))
			{
			if (isupper(boardFEN->piecePlacement[i]))
				printf(ANSI_COLOR_CYAN	" %c "	ANSI_COLOR_RESET, boardFEN->piecePlacement[i]);
			else if islower((boardFEN->piecePlacement[i]))
				printf(ANSI_COLOR_RED	" %c "	ANSI_COLOR_RESET, boardFEN->piecePlacement[i]);
			}
	}
		printf("|\n  |-------------------------------|\n\n");		
		printFEN(boardFEN);
}

void boardToFEN(struct FEN* boardFEN, struct boardSquare* board)
{
	int emptyCounter = 0;
	char newFEN[100];
	int lenFEN = 0; 
	
	for(int i = 0; i<64; i++)
	{
		if( i==8 || i==16 || i==24 || i==32 || i==40 || i==48 || i==56 )
		{
			if(emptyCounter != 0)
			{
				newFEN[lenFEN] = (char) emptyCounter + '0';
				lenFEN++;
				emptyCounter = 0;
			}
			newFEN[lenFEN]='/';
			lenFEN++;
		}

		if(isalpha(board[i].piece))
		{
			if(emptyCounter != 0)
			{
				newFEN[lenFEN] = (char) emptyCounter + '0';
				lenFEN++;
				emptyCounter = 0;
			}
			
			newFEN[lenFEN] = board[i].piece;
			lenFEN++;
		}
		else
			emptyCounter++;
		
	}
	
	if(emptyCounter != 0)
	{
				newFEN[lenFEN] = (char) emptyCounter + '0';
				lenFEN++;
	}
	newFEN[lenFEN]='\0';
	strcpy(boardFEN->piecePlacement,newFEN);
}

char gameDriver(struct FEN* boardFEN, struct boardSquare* board)
{
	//allows conversion from algebraic notation i.e. a7, to a number in the array, a = 0, b = 1 etc
	char winState;
	int noWinner = 1;
	int LETTERTONUM = 97;
	//boardToFEN(boardFEN,board);
	printBoardFEN(boardFEN);
	char currentMove[10];
	int squareNumFrom = 0;
	int squareNumTo = 0;
	
	
	while(noWinner)
	{
		
		scanf("%s",currentMove);
		
		if( strncmp(currentMove,"quit",4) == 0 || strncmp(currentMove,"exit",4) == 0 )
			return winState;

		//converting from algebraic notation to array placement
		squareNumFrom += (int) currentMove[0] - LETTERTONUM;
		squareNumFrom += 8 * abs(((int) currentMove[1] - '0') - 8);
		squareNumTo += (int) currentMove[2] - LETTERTONUM;
		squareNumTo += 8 * abs(((int) currentMove[3] - '0') - 8);
		
		if(squareNumFrom >= 0 && squareNumFrom < 64 && squareNumTo >= 0 && squareNumTo < 64)
		{
			//disallow taking of your own pieces
			if((isupper(board[squareNumFrom].piece) && isupper(board[squareNumTo].piece)) || (islower(board[squareNumFrom].piece) && islower(board[squareNumTo].piece)))
			{
				printf("Invalid Move, Enter new move\n");
				squareNumFrom = 0;
				squareNumTo = 0;
			}
			else
			{
				board[squareNumTo].piece = board[squareNumFrom].piece;
				board[squareNumFrom].piece = '\0';
				squareNumFrom = 0;
				squareNumTo = 0;
				boardToFEN(boardFEN,board);
				printBoardFEN(boardFEN);
			}
		}
	
		//boardToFEN(boardFEN,board);
		//printBoardFEN(boardFEN);
		
		
		
	}
	winState = 's';
	return winState;
}

int main(int argc, char** argv)	
{	
	char winState;
	//initialize the board to starting position
	static struct FEN boardStruct = {0};
	struct FEN *boardFEN = &boardStruct;
	strcpy(boardStruct.piecePlacement,"rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR");
	boardStruct.activeColor = 'w';
	strcpy(boardStruct.castling,"KQkq");
	strcpy(boardStruct.enPassant,"-");
	boardStruct.halfMoveClock = 0;
	boardStruct.fullMoveNumber = 1;
	
	//takes in FEN string as run parameters
	//ensure spaces are between each section, and en passent is a '-' if it is not currently applicable
	
	if(argc == 7)
	{
		strcpy(boardStruct.piecePlacement, argv[1]);
		boardStruct.activeColor = *argv[2];
		strcpy(boardStruct.castling, argv[3]);
		strcpy(boardStruct.enPassant, argv[4]);
		boardStruct.halfMoveClock = (int) *argv[5] - '0';
		boardStruct.fullMoveNumber = (int) *argv[6] - '0';
	}
	//declare and connect all board-squares to create a usable platform
	struct boardSquare* board = malloc(64* sizeof(struct boardSquare));
	
	//sets up the board, but the edge ring has to have special care so the board does not loop around
	//instead the edges should not point to anything
	for(int i = 0; i < 64; i++)
	{
		board[i].piece = '\0';
		if(i-8 >= 0)
			board[i].up = &board[i-8];
		if(i+8 < 64)
			board[i].down = &board[i+8];
		if(i-1 >= 0)
			board[i].left = &board[i-1];
		if(i+1 < 64)
			board[i].right = &board[i+1];
	}
	//top row - shouldn't point anywhere going up
	for(int i = 0; i<8; i++)
		board[i].up = NULL;
	//bottom row - shouldn't point anywhere down
	for(int i = 56; i<64; i++)
		board[i].down = NULL;
	//left-most column - shouldn't point anywhere left (board shouldn't loop around)
	for(int i = 0; i<57; i+=8)
		board[i].left = NULL;
	//right-most column - shouldn't point anywhere right (board shouldn't loop around)
	for(int i = 7; i<64; i+=8)
		board[i].right = NULL;
	
	//takes initial state and loads it into the data-structure so the game can be played
	FENtoBoard(boardFEN,board);
	
	//plays the game in a loop until there is a victor, returns 'w' if white wins, 'b' if black wins and 's' if there is a stalemate
	winState = gameDriver(boardFEN,board);
	
	if(winState == 'w')
		printf("Checkmate, White Wins\n");
	else if (winState == 'b')
		printf("Checkmate, Black Wins\n");
	else if (winState == 's')
		printf("Stalemate\n");
	
	return 0;
}