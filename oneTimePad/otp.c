/*********************************************
Sean Jamison
sean.jamison@rocketmail.com

One-Time-Pad encryption/decryption program

more information within the README and here
https://en.wikipedia.org/wiki/One-time_pad
**********************************************/

#include <stdio.h>
#include <string.h>
#include <ctype.h>
 
#define charToIndex 'a'



//takes 2 characters, converts the number to its alphabetic index (a = 1, b = 2 ... etc) and then exclusively or's these numbers together
//it does this in modular fashion so if the number is out of index it is returned to 26 or less, then that index is converted back to a 
//letter and returned, this is the encryption/decryption algorithm

char endecrypt(char plain, char pad)
{
	int cypher = (int) plain - 'a';
	int encrypt = (int) pad - 'a';
	cypher = (cypher ^ encrypt);
	cypher = cypher + charToIndex;
	return (char) cypher;
}


int main()
{	
	int properInputPad = 1;
	int properInputPlain = 1;
	char plainInput[256] = "\0";
	char padInput[256] = "\0";
	char plain[256] = "\0";
	char pad[256] = "\0";
	char cypher[256] = "\0";
	char* noSpace;
	
	printf("Enter plaintext or cyphertext, then press ENTER...\n");
	scanf ("%[^\n]%*c", plainInput);
	printf("Enter one-time-pad then press ENTER...\n");
	scanf ("%[^\n]%*c", padInput);	

	noSpace = strtok(plainInput," ");
	while(noSpace != NULL)
	{
		strcat(plain,noSpace);
		noSpace = strtok(NULL," ");
	}
	
	noSpace = strtok(padInput," ");
	while(noSpace != NULL)
	{
		strcat(pad,noSpace);
		noSpace = strtok(NULL," ");
	}
		
	if(strlen(pad) >= strlen(plain))
	{
		for(int i = 0; i < strlen(plain); i++)
			cypher[i] = endecrypt(plain[i],pad[i]);
		cypher[strlen(plain)] = '\0';
		printf("%s\n",cypher);
	}
	else
		printf("The pad is not of sufficient length\n");	
	return 0;
}

